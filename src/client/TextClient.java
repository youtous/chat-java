/**
 *
 */
package client;

import java.io.IOException;
import java.util.*;

/**
 * Interface utilisateur d'un client en mode texte
 */
public class TextClient implements Observer {

    /**
     * Contain the ignored clients
     */
    private ArrayList<String> ignoredClients = new ArrayList<>();
    /**
     * Contain the clients that ignore us
     */
    private ArrayList<String> ignoredByClients = new ArrayList<>();

    /* (non-Javadoc)
     * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
     */
    @SuppressWarnings("incomplete-switch")
    @Override
    public void update(Observable o, Object arg) {
        Event event = (Event) arg;

        switch (event.type) {
            case CONNECTION_ENDED:
                System.out.println("*** Vous êtes maintenant déconnecté ***");
                System.exit(0);
                break;
            case MESSAGE:
                String user = event.data[0].split(">")[0];
                if (!this.ignoredClients.contains(user)) {
                    System.out.println(event.data[0]);
                }
                break;
            case PRIVATE:
                if (!this.ignoredClients.contains(event.data[0])) {
                    System.out.println("*** Message privé de " + event.data[0] + " :");
                    System.out.println(String.join(" ", Arrays.copyOfRange(event.data, 1, event.data.length)));
                    System.out.println("*** Fin du message");
                }
                break;
            case ALIAS_UPDATED:
                System.out.println("*** Vous êtes maintenant connecté avec le pseudonyme " + event.data[0] + " ***");
                break;
            case ERROR:
                System.err.println("ERREUR - " + event.data[0]);
                break;
            case USER_CONNECTED:
                System.out.println("[" + event.data[0] + "] s'est connecté.");
                break;
            case USER_DISCONNECTED:
                System.out.println("[" + event.data[0] + "] s'est déconnecté.");
                break;
            case LIST:
                String[] clients = event.data[0].split(" ");
                for (int i = 0; i < clients.length; i++) {
                    String username = clients[i];
                    if (this.ignoredClients.contains(username)) {
                        clients[i] = "(" + clients[i] + ")";
                    }
                    if (this.ignoredByClients.contains(username)) {
                        clients[i] = "[" + clients[i] + "]";
                    }
                }
                System.out.println("*** Clients connectés : " + String.join(" ", clients) + " ***");
                break;
            case USER_RENAMED:
                System.out.println("[" + event.data[0] + "] est maintenant connu sous : " + event.data[1]);
                break;
            case IGNORE:
                if (!this.ignoredClients.contains(event.data[0])) {
                    this.ignoredClients.add(event.data[0]);
                    System.out.println("*** " + event.data[0] + " est désormais ignoré ***");
                } else {
                    System.out.println("*** " + event.data[0] + " est déjà ignoré ***");
                }
                break;
            case IGNORED_BY:
                if (!this.ignoredByClients.contains(event.data[0])) {
                    this.ignoredByClients.add(event.data[0]);
                    System.out.println("*** " + event.data[0] + " vous a ignoré ***");
                }
                break;
            case UNIGNORE:
                if (this.ignoredClients.contains(event.data[0])) {
                    this.ignoredClients.remove(event.data[0]);
                    System.out.println("*** " + event.data[0] + " est désormais dé-ignoré ***");
                } else {
                    System.out.println("*** " + event.data[0] + " n'est pas ignoré ***");
                }
                break;
            case UNIGNORED_BY:
                if (this.ignoredByClients.contains(event.data[0])) {
                    this.ignoredByClients.remove(event.data[0]);
                    System.out.println("*** " + event.data[0] + " vous a dé-ignoré ***");
                }
                break;
        }
    }

    /**
     * Programme principal
     *
     * @param args arguments de ligne de commande
     */
    public static void main(String[] args) {
        try (Scanner input = new Scanner(System.in)) {

            System.out.println("Entrez votre identifiant :");
            String alias = input.nextLine();

            Connection connection = new Connection("localhost", 3333, alias, new TextClient());

            while (true) {
                // lecture des entrées e l'utilisateur
                String message = input.nextLine();
                // si le message comporte autre chose que des caractères blancs...
                if (!message.trim().equals(""))
                    // ...il est envoyé au serveur
                    connection.sendToServer(message);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
