/**
 *
 */
package client;

/**
 * Définition des différents types d'événements pouvant être émis par une connexion
 */
public enum EventType {
    /**
     * l'événement correspond à un changement du pseudonyme de ce client
     * <p>
     * <br><p>Données de l'événement : nouveau pseudonyme
     */
    ALIAS_UPDATED,
    /**
     * l'événement est émis lorsque la connexion avec le serveur est terminée
     * <p>
     * <br><p>Données de l'événement : aucune
     */
    CONNECTION_ENDED,
    /**
     * l'événement est émis lorsque le serveur envoie une commande d'erreur
     * <p>
     * <br><p>Données de l'événement : identifiant de l'erreur
     */
    ERROR,
    /**
     * l'événement est émis lorsque le serveur envoie la liste des utilisateurs
     * <p>
     * <br><p>Données de l'événement : pseudonymes des utilisateurs connectés
     */
    LIST,
    /**
     * l'événement est émis lorsque le serveur envoie un message "normal"
     * <p>
     * <br><p>Données de l'événement : message
     */
    MESSAGE,
    /**
     * l'événement est émis lorsque le serveur signale la connexion d'un nouvel utilisateur
     * <p>
     * <br><p>Données de l'événement : pseudonyme du nouvel utilisateur
     */
    USER_CONNECTED,
    /**
     * l'événement est émis lorsque le serveur signale la déconnexion d'un utilisateur
     * <p>
     * <br><p>Données de l'événement : pseudonyme de l'utilisateur déconnecté
     */
    USER_DISCONNECTED,
    /**
     * l'événement est émis lorsque le serveur signale le changement de pseudonyme d'un utilisateur
     * <p>
     * <br><p>Données de l'événement : ancien pseudonyme, nouveau pseudonyme
     */
    USER_RENAMED,
    /**
     * l'événement est émis lorsque le serveur signale un message privé provenant d'un utilisateur
     * <p>
     * <br><p>Données de l'événement : l'éméteur, le message
     */
    PRIVATE,
    /**
     * l'événement est émis lorsque le serveur valide la possibilité d'ignorer un utilisateur
     * <p>
     * <br><p>Données de l'événement : l'utilisateur ignoré
     */
    IGNORE,
    /**
     * l'événement est émis lorsqu'un utilisateur vient de nous ignorer
     * <p>
     * <br><p>Données de l'événement : l'utilisateur ignorant
     */
    IGNORED_BY,
    /**
     * l'événement est émis lorsque le serveur valide la possibilité de dé-ignorer un utilisateur
     * <p>
     * <br><p>Données de l'événement : l'utilisateur dé-ignoré
     */
    UNIGNORE,
    /**
     * l'événement est émis lorsqu'un utilisateur vient de nous dé-ignorer
     * <p>
     * <br><p>Données de l'événement : l'utilisateur dé-ignorant
     */
    UNIGNORED_BY,
}