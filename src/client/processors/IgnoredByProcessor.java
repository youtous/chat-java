package client.processors;

import client.Connection;
import client.EventType;

public class IgnoredByProcessor implements CommandProcessor {

    @Override
    public void processCommand(String args, Connection connection) {
        connection.emitEvent(EventType.IGNORED_BY, args);
    }

}
