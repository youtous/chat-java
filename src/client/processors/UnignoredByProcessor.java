package client.processors;

import client.Connection;
import client.EventType;

public class UnignoredByProcessor implements CommandProcessor {

    @Override
    public void processCommand(String args, Connection connection) {
        connection.emitEvent(EventType.UNIGNORED_BY, args);
    }

}
