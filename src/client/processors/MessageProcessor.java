/**
 *
 */
package client.processors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import client.Connection;
import client.EventType;

/**
 * Classe utilisée par une connexion pour traiter les messages reçus du serveur
 */
public class MessageProcessor {
    /**
     * Processeurs de commandes.
     */
    private HashMap<String, CommandProcessor> commandProcessors;

    /**
     * Contructeur.
     */
    public MessageProcessor() {
        commandProcessors = new HashMap<>();
        // Client commands
        this.commandProcessors.put("#alias", new AliasProcessor());
        this.commandProcessors.put("#list", new ListProcessor());
        this.commandProcessors.put("#ignore", new IgnoreProcessor());
        this.commandProcessors.put("#unignore", new UnignoreProcessor());

        // Server sent commands
        this.commandProcessors.put("#connected", new ConnectedProcessor());
        this.commandProcessors.put("#disconnected", new DisconnectedProcessor());
        this.commandProcessors.put("#renamed", new RenamedProcessor());
        this.commandProcessors.put("#private", new PrivateProcessor());
        this.commandProcessors.put("#ignoredby", new IgnoredByProcessor());
        this.commandProcessors.put("#unignoredby", new UnignoredByProcessor());
    }

    /**
     * Traite un message en provenance du serveur. En fonction de la nature de celui-ci (commande,
     * message "normal), différents événements pourront être émis.
     *
     * @param message    message à traiter
     * @param connection connexion ayant reçu le message
     */
    public void processMessage(String message, Connection connection) {
        message = message.trim();
        if (message.startsWith("#")) {
            //String regex = "(\\#\\w+\\s)((?:\\w+\\s?)+)";
            String[] args = message.split(" ");

            if (this.commandProcessors.containsKey(args[0])) {
                if (args.length >= 2) {
                    args[1] = String.join(" ", Arrays.copyOfRange(args, 1, args.length));
                }
                this.commandProcessors.get(args[0]).processCommand(args[1], connection);
            } else {
                connection.emitEvent(EventType.ERROR, "Unknown command : " + String.join(" ", Arrays.copyOfRange(args, 0, args.length)));
            }
        } else {
            connection.emitEvent(EventType.MESSAGE, message);
        }
    }

}
