package client.processors;

import client.Connection;
import client.EventType;

public class UnignoreProcessor implements CommandProcessor {

    @Override
    public void processCommand(String args, Connection connection) {
        connection.emitEvent(EventType.UNIGNORE, args);
    }

}
