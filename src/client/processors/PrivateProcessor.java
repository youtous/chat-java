package client.processors;

import client.Connection;
import client.EventType;

public class PrivateProcessor implements CommandProcessor  {

	@Override
	public void processCommand(String args, Connection connection) {
        connection.emitEvent(EventType.PRIVATE, args.split(" "));
	}

}
